#include <cstdio>
#include <cstdlib>

int main(int argc, char* argv[])
{
	int total, ofertas, valor, duracao, i= 0, result = 0;	//tempo total, número de ofertas, valor de oferta, tempo de oferta, auxiliar, resultado final
	fscanf(stdin,"%d %d",&total,&ofertas);					//lê tempo total e número de ofertas
	while(i<ofertas && total>0)								//enquanto há ofertas para ler e não ultrapassou o tempo disponível
	{
		fscanf(stdin,"%d %d",&valor,&duracao);				//lê valor da oferta e respetiva duração
		if(duracao <= total)								//se não ultrapassa o tempo disponível
		{
			result += valor;								//adiciona valor da oferta ao resultado final
			total -= duracao;								//o tempo disponível diminui
		}
		i++;												//aumenta número de ofertas já lidas
	}
	fprintf(stdout,"%d\n",result);							//imprime o resultado final, ou seja, o ganho da TVI usando este método
	return 0;
}
