#include <cstdio>
#include <clocale>
#include <cstdlib>
#include <windows.h>

int* tempos;												//
int* valores;												//
int tempoTotal = 0;											//tempo disponível para anúncios
int ofertas = 0;											//número de ofertas

int best(int,int,int);

int main(int argc, char* argv[])
{
	setlocale(LC_ALL,"");
	fscanf(stdin,"%d %d",&tempoTotal,&ofertas);				//recebe o tempo disponível e o número de ofertas
	tempos = (int*)calloc(ofertas,sizeof(int));
	valores = (int*)calloc(ofertas,sizeof(int));
	for(int i=0;i<ofertas;i++)
	{
		fscanf(stdin,"%d %d",valores+i,tempos+i);			//preenche os arrays, lendo tempo e valor de cada anúncio
	}
	LARGE_INTEGER freq, t0, tF;
	double elapsedTime;
	QueryPerformanceFrequency(&freq);
	QueryPerformanceCounter(&t0);
	int result = best(0,0,tempoTotal);
	QueryPerformanceCounter(&tF);
	elapsedTime = (double)(tF.QuadPart - t0.QuadPart) / (double) freq.QuadPart;
	fprintf(stdout,"%lf\n",elapsedTime*1000000);
	//fprintf(stdout,"%d\n",result);			//imprime o resultado final
	free(tempos);											//libertar memória
	free(valores);
	return 0;
}

int best(int soma, int pos, int resto)
{
	if(pos==ofertas)										//já verificámos todos os anúncios, não podemos continuar
	{
		return 0;
	}
	int naoUsado = best(soma,pos+1,resto);					//caso em que o anúncio atual não é usado
	int usado;
	if(resto-*(tempos+pos)>=0)								//se o anúncio atual puder ser usado
	{
		resto-=*(tempos+pos);								//atualiza o tempo restante, ou seja, subtrai o tempo do anúncio atual
		soma+=*(valores+pos);								//aumenta o ganho com o anúncio atual em uso
		if(resto == 0)
		{
			usado = *(valores+pos);							//se este é o último anúncio que pode ser usado, calculamos o máximo com o seu valor

		}
		else if(resto>0)
		{
			usado = *(valores+pos)+best(soma,pos+1,resto);	//se são adicionados mais anúncios, calculamos o máximo com o máximo usando mais anúncios
		}
	}
	else
	{
		usado = 0;											//queríamos usar o anúncio atual, mas ultrapassa o tempo disponível
	}
	return naoUsado>usado?naoUsado:usado;					//devolve o máximo dos dois valores
}
