#include <cstdio>
#include <clocale>
#include <cstdlib>
#include <windows.h>

int main(int argc, char* argv[])
{
	setlocale(LC_ALL,"");
	LARGE_INTEGER freq, t0, tF;
	double elapsedTime;
	QueryPerformanceFrequency(&freq);
	QueryPerformanceCounter(&t0);
	int total, ofertas, valor, duracao, i= 0, result = 0;	//tempo total, número de ofertas, valor de oferta, tempo de oferta, auxiliar, resultado final
	fscanf(stdin,"%d %d",&total,&ofertas);					//lê tempo total e número de ofertas
	while(i<ofertas && total>0)								//enquanto há ofertas para ler e não ultrapassou o tempo disponível
	{
		fscanf(stdin,"%d %d",&valor,&duracao);				//lê valor da oferta e respetiva duração
		if(duracao <= total)								//se não ultrapassa o tempo disponível
		{
			result += valor;								//adiciona valor da oferta ao resultado final
			total -= duracao;								//o tempo disponível diminui
		}
		i++;												//aumenta número de ofertas já lidas
	}
	QueryPerformanceCounter(&tF);
	elapsedTime = (double)(tF.QuadPart - t0.QuadPart) / (double) freq.QuadPart;
	//fprintf(stdout,"T: %lf microssegundos\n", elapsedTime*1000000);
	//fprintf(stdout,"%d\n",result);							//imprime o resultado final, ou seja, o ganho da TVI usando este método
	fprintf(stdout,"%lf\n",elapsedTime*1000000);
	return 0;
}
