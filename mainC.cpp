#include <cstdio>
#include <cstdlib>

int main(int argc, char* argv[])
{
	int tempoTotal, ofertas;
	fscanf(stdin,"%d %d",&tempoTotal,&ofertas);										//obtém valores para o tempo disponível e para o número de ofertas
	int* valores = (int*)malloc(ofertas*sizeof(int));
	int* tempos = (int*)malloc(ofertas*sizeof(int));
	for(int i=0;i<ofertas;i++)
	{
		fscanf(stdin,"%d %d",valores+i,tempos+i);									//cria arrays para as ofertas e preenche-os
	}
	int* linhaAnterior = (int*)calloc(tempoTotal+1,sizeof(int));							//em vez de uma matriz, basta a linha atual e a anterior (que começa com 0s)
	int* linhaAtual = (int*)calloc(tempoTotal+1,sizeof(int));
	int result = 0;
	for(int i=0;i<ofertas;i++)
	{
		for(int j=1;j<=tempoTotal;j++)
		{
			if(j<tempos[i])
			{
				linhaAtual[j] = linhaAnterior[j];
			}
			else
			{
				if(linhaAnterior[j-tempos[i]] + valores[i] > linhaAnterior[j])
				{
					linhaAtual[j] = linhaAnterior[j-tempos[i]] + valores[i];
					result = (linhaAtual[j]>result?linhaAtual[j]:result);
				}
				else
				{
					linhaAtual[j] = linhaAnterior[j];
				}
			}
		}
		int* aux = linhaAnterior;
		linhaAnterior = linhaAtual;
		linhaAtual = aux;
	}
	fprintf(stdout,"%d\n",result);												//imprimir resultado
	free(valores);																//libertar memória
	free(tempos);
	free(linhaAnterior);
	free(linhaAtual);
	return 0;
}
